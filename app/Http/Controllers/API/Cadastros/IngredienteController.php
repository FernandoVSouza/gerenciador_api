<?php

namespace App\Http\Controllers\API\Cadastros;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cadastros\Ingrediente;

class IngredienteController extends Controller
{
    private function getIngredienteModel() {
        return new Ingrediente();
    }

    public function index()
    {
        try {
            return $this->getIngredienteModel()->getIngredientesAll();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function store(Request $request)
    {
        try {
            return $this->getIngredienteModel()->saveIngrediente($request);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function show($id_ingrediente)
    {
        try {
            return $this->getIngredienteModel()->getIngrediente($id_ingrediente);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function update(Request $request, $id_ingrediente)
    {
        try {
            return $this->getIngredienteModel()->updateIngrediente($request, $id_ingrediente);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function ativar($id_ingrediente)
    {
        try {
            return $this->getIngredienteModel()->ativar($id_ingrediente);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function desativar($id_ingrediente)
    {
        try {
            return $this->getIngredienteModel()->desativar($id_ingrediente);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers\API\Cadastros;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cadastros\Produto;

class ProdutoController extends Controller
{
    private function getProdutoModel() {
        return new Produto();
    }

    public function index()
    {
        try {
            return $this->getProdutoModel()->getProdutosAll();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function store(Request $request)
    {
        try {
            return $this->getProdutoModel()->saveProduto($request);
        } catch (\FatalThrowableError $ex) {
            throw new \FatalThrowableError($ex->getMessage());
        }
    }

    public function show($id_incremento)
    {
        try {
            return $this->getProdutoModel()->getIncremento($id_incremento);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function update(Request $request, $id_incremento)
    {
        try {
            return $this->getProdutoModel()->updateIncremento($request, $id_incremento);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function ativar($id_incremento)
    {
        try {
            return $this->getProdutoModel()->ativar($id_incremento);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function desativar($id_incremento)
    {
        try {
            return $this->getProdutoModel()->desativar($id_incremento);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}

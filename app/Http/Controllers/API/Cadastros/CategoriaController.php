<?php

namespace App\Http\Controllers\API\Cadastros;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cadastros\Categoria;

class CategoriaController extends Controller
{
    private function getCategoriaModel() {
        return new Categoria();
    }

    public function index()
    {
        try {
            return $this->getCategoriaModel()->getCategoriasAll();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function store(Request $request)
    {
        try {
            return $this->getCategoriaModel()->saveCategoria($request);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function show($id_categoria)
    {
        try {
            return $this->getCategoriaModel()->getCategoria($id_categoria);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function update(Request $request, $id_categoria)
    {
        try {
            return $this->getCategoriaModel()->updateCategoria($request, $id_categoria);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function ativar($id_categoria)
    {
        try {
            return $this->getCategoriaModel()->ativar($id_categoria);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function desativar($id_categoria)
    {
        try {
            return $this->getCategoriaModel()->desativar($id_categoria);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}

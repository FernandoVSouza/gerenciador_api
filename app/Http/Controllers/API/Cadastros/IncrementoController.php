<?php

namespace App\Http\Controllers\API\Cadastros;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cadastros\Incrementos;

class IncrementoController extends Controller
{
    private function getIncrementoModel() {
        return new Incrementos();
    }

    public function index()
    {
        try {
            return $this->getIncrementoModel()->getIncrementosAll();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function store(Request $request)
    {
        try {
            return $this->getIncrementoModel()->saveIncremento($request);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function show($id_incremento)
    {
        try {
            return $this->getIncrementoModel()->getIncremento($id_incremento);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function update(Request $request, $id_incremento)
    {
        try {
            return $this->getIncrementoModel()->updateIncremento($request, $id_incremento);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function ativar($id_incremento)
    {
        try {
            return $this->getIncrementoModel()->ativar($id_incremento);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function desativar($id_incremento)
    {
        try {
            return $this->getIncrementoModel()->desativar($id_incremento);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}

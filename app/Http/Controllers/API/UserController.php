<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Carbon\Carbon;

class UserController extends Controller
{
    public $successStatus = 200;

    public function login(){ 
        try {
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                $user = Auth::user(); 

                if($user->ativo) {
                    $tokenResult = $user->createToken('Admin');
                    $token = $tokenResult->token;
                    $token->expires_at = Carbon::now()->addDay(1);
                    $token->save();

                    $user->api_token = $tokenResult->accessToken;
                    $user->save();

                    return response()->json([
                        'access_token' => $tokenResult->accessToken,
                        'token_type' => 'Bearer',
                        'expires_at' => Carbon::parse( $tokenResult->token->expires_at)->toDateTimeString(),
                        'user' => $user
                    ]);
                } else {
                    return response()->json(['error' => 'Usuário Não Encontrado'], 401);
                }
            } else{ 
                return response()->json(['error' => 'Unauthorised'], 401); 
            }
        } catch (\FatalThrowableError1 $ex) {
            throw response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    public function loginApp(){ 
        try {
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                $user = Auth::user(); 

                if($user->ativo && $user->tipo == 1) {
                    $tokenResult = $user->createToken('Admin');
                    $token = $tokenResult->token;
                    $token->expires_at = Carbon::now()->addDay(1);
                    $token->save();

                    $user->api_token = $tokenResult->accessToken;
                    $user->save();

                    return response()->json([
                        'access_token' => $tokenResult->accessToken,
                        'token_type' => 'Bearer',
                        'expires_at' => Carbon::parse( $tokenResult->token->expires_at)->toDateTimeString(),
                        'user' => $user
                    ]);
                } else {
                    return response()->json(['error' => 'Usuário Não Encontrado'], 401);
                }
            } else{ 
                return response()->json(['error' => 'Unauthorised'], 401); 
            }
        } catch (\FatalThrowableError $ex) {
            throw response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    public function register(Request $request) 
    { 
        try {

            $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'email' => 'required|email', 
                'password' => 'required', 
                'c_password' => 'required|same:password',
                'phone' => 'required', 
                'tipo' => 'required', 
            ]);

            $user = new User([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'phone' => $request->phone,
                'tipo' => $request->tipo,
                'ativo' => 0
            ]);

            if ($validator->fails()) return response()->json(['error'=>$validator->errors()], 401);            
            
            $user->save();
            
            return response()->json([ 'message' => 'Cadastrado com sucesso!' ], 201);

        } catch (\Throwable $e) {
            // throw response()->json(['error' => $ex->getMessage()], 500);
            throw new \Exception($e->getMessage()."\n".$e->getTraceAsString(), $e->getCode());
        }
    }

    public function details(Request $request) 
    {
        try {
            $user = Auth::user();
            return response()->json($request->user(), 200); 
        } catch (\Exception $ex) {
            throw response()->json(['error' => $ex->getMessage()], 500);
        }
    } 
}
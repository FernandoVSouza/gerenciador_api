<?php

namespace App\Models\Cadastros;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cadastros\Ingrediente;
use Illuminate\Support\Facades\DB;

class Produto extends Model
{
    protected $table = 'produtos';
    protected $primaryKey = 'id_produto';

    private function produtoPopulate($request) {
        $this->fk_categoria = $request->fk_categoria;
        $this->descricao = $request->descricao;
        $this->preco_unitario = $request->preco_unitario;

        return $this;
    }

    public function saveProduto($request) {
        try {
            DB::beginTransaction();
            $produto = $this->produtoPopulate($request);
            $produto->ativo = 1;
            $produto->save();

            if ($produto['id_produto']) {

                foreach ($request->ingredientes as $ingrediente) {
                    DB::table('ingredientes_produtos')
                        ->insert(
                            [
                                'fk_ingrediente' => $ingrediente,
                                'fk_produto' => $produto->id_produto,
                                'created_at' => \Carbon\Carbon::now(),
                                'updated_at' => \Carbon\Carbon::now()
                            ]
                        );
                }
            }
            DB::commit();
        } catch (\FatalThrowableError $ex) {
            DB::rollBack();
            throw new \FatalThrowableError($ex->getMessage());
        }
    }

    public function updateProduto($request, $id_produto) {
        try {
            $produto = $this::find($id_produto);
            $produto->produtoPopulate($request);
            $produto->save();
            return $produto;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function getProduto($id_produto) {
        try {
            $produto = $this::select(
                'produtos.id_produto',
                'produtos.descricao',
                'categorias.descricao as categoria',
                'produtos.preco_unitario',
                'produtos.ativo',
                'produtos.created_at',
                'produtos.updated_at'
            )
            ->where('produtos.ativo', '=', 1)
            ->join('categorias', 'categorias.id_categoria', '=', 'produtos.fk_categoria')
            ->first();

            if($produto) {
                $ingredientes = Ingrediente::select(
                    'ingredientes.*'
                )
                ->join('ingredientes_produtos', 'ingredientes_produtos.fk_produto', '=', $produto->id_produto)
                ->where('ingredientes.ativo', 1)
                ->get();

                $produto->ingredientes = $ingredientes;
            }

            return $produto;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function getProdutosAll() {
        try {
            $produtos = $this::select(
                'produtos.id_produto',
                'produtos.descricao',
                'categorias.descricao as categoria',
                'produtos.preco_unitario',
                'produtos.ativo',
                'produtos.created_at',
                'produtos.updated_at'
            )
            ->where('produtos.ativo', '=', 1)
            ->join('categorias', 'categorias.id_categoria', '=', 'produtos.fk_categoria')
            ->get();

            foreach ($produtos as &$produto) {
                if($produto) {
                    $ingredientes = Ingrediente::select(
                        'ingredientes.*'
                    )
                    ->join('ingredientes_produtos', 'ingredientes_produtos.fk_produto', '=', 'produtos.id_produto')
                    ->where('ingredientes.ativo', 1)
                    ->get();
    
                    $produto->ingredientes = $ingredientes;
                }
            }
            return $produtos;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function ativar($id_produto)
    {
        try {
            $produto = $this::find($id_produto);
            $produto->ativo = 1;
            $produto->save();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function desativar($id_produto)
    {
        try {
            $produto = $this::find($id_produto);
            $produto->ativo = 0;
            $produto->save();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}

<?php

namespace App\Models\Cadastros;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    protected $table = 'ingredientes';
    protected $primaryKey = 'id_ingrediente';

    private function ingredientePopulate($request) {
        $this->descricao = $request->descricao;
        $this->preco = $request->preco;
        $this->incremento = $request->incremento;

        return $this;
    }

    public function saveIngrediente($request) {
        try {
            $ingrediente = $this->ingredientePopulate($request);
            $ingrediente->ativo = 1;
            $ingrediente->save();
            return $ingrediente;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function updateIngrediente($request, $id_ingrediente) {
        try {
            $ingrediente = $this::find($id_ingrediente);
            $ingrediente->ingredientePopulate($request);
            $ingrediente->save();
            return $ingrediente;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function getIngrediente($id_ingrediente) {
        try {
            $ingrediente = $this::find($id_ingrediente);
            return $ingrediente;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function getIngredientesAll() {
        try {
            $ingredientes = $this::all();
            return $ingredientes;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function ativar($id_ingrediente)
    {
        try {
            $ingrediente = $this::find($id_ingrediente);
            $ingrediente->ativo = 1;
            $ingrediente->save();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function desativar($id_ingrediente)
    {
        try {
            $ingrediente = $this::find($id_ingrediente);
            $ingrediente->ativo = 0;
            $ingrediente->save();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}

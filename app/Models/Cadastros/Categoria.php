<?php

namespace App\Models\Cadastros;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';
    protected $primaryKey = 'id_categoria';

    private function categoriaPopulate($request) {
        $this->descricao = $request->descricao;
        $this->alteracao = $request->alteracao;
        $this->incremento = $request->incremento;

        return $this;
    }

    public function saveCategoria($request) {
        try {
            $categoria = $this->categoriaPopulate($request);
            $categoria->ativo = 1;
            $categoria->save();
            return $categoria;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function updateCategoria($request, $id_categoria) {
        try {
            $categoria = $this::find($id_categoria);
            $categoria->categoriaPopulate($request);
            $categoria->save();
            return $categoria;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function getCategoria($id_categoria) {
        try {
            $categoria = $this::find($id_categoria);
            return $categoria;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function getCategoriasAll() {
        try {
            $categorias = $this::all();
            return $categorias;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function ativar($id_categoria)
    {
        try {
            $categoria = $this::find($id_categoria);
            $categoria->ativo = 1;
            $categoria->save();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function desativar($id_categoria)
    {
        try {
            $categoria = $this::find($id_categoria);
            $categoria->ativo = 0;
            $categoria->save();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}

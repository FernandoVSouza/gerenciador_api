<?php

use Illuminate\Http\Request;

Route::post('login', 'API\UserController@login');
Route::post('loginApp', 'API\UserController@loginApp');
Route::post('register', 'API\UserController@register');

// Route::group(['middleware' => 'auth:api', 'cors'], function() {

    //---------- CATEGORIA ------------ //
    Route::get('categoria/ativar/{id_categoria}', 'API\Cadastros\CategoriaController@ativar');
    Route::get('categoria/desativar/{id_categoria}', 'API\Cadastros\CategoriaController@desativar');
    Route::resource('categoria', 'API\Cadastros\CategoriaController');

    Route::get('details', 'API\UserController@details');
    //---------- INGREDIENTE ------------ //
    Route::resource('ingrediente', 'API\Cadastros\IngredienteController');
    Route::get('ingrediente/ativar/{id_ingrediente}', 'API\Cadastros\IngredienteController@ativar');
    Route::get('ingrediente/desativar/{id_ingrediente}', 'API\Cadastros\IngredienteController@desativar');

    //---------- PRODUTO ------------ //
    Route::resource('produto', 'API\Cadastros\ProdutoController');
    Route::get('produto/ativar/{id_produto}', 'API\Cadastros\ProdutoController@ativar');
    Route::get('produto/desativar/{id_produto}', 'API\Cadastros\ProdutoController@desativar');

// });
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIngredientesProdutos extends Migration
{
    public function up()
    {
        Schema::create('ingredientes_produtos', function (Blueprint $table) {
            $table->increments('id_ingrediente_produto');
            $table->unsignedInteger('fk_ingrediente');
            $table->unsignedInteger('fk_produto');
            $table->timestamps();

            $table->foreign('fk_ingrediente')->references('id_ingrediente')->on('ingredientes');
            $table->foreign('fk_produto')->references('id_produto')->on('produtos');
            
        });
    }

    public function down()
    {
        Schema::dropIfExists('ingredientes_produtos');
    }
}

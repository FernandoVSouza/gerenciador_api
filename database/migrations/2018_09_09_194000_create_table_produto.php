<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduto extends Migration
{
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id_produto');
            $table->unsignedInteger('fk_categoria');
            $table->string('descricao');
            $table->decimal('preco_unitario');
            $table->boolean('ativo');
            $table->timestamps();

            $table->foreign('fk_categoria')->references('id_categoria')->on('categorias');
            
        });
    }

    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategoria extends Migration
{
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table) {
            $table->increments('id_categoria');
            $table->string('descricao');
            $table->boolean('alteracao');
            $table->boolean('incremento');
            $table->boolean('ativo');
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('categorias');
    }
}
